# Documentación Técnica

## Versión 1
### Circuito electrónico v1

La placa del circuito electrónico integra los componentes para permitir su funcionamiento.

La versión 1 del sensor utiliza un controlador Arduino Pro Mini, un sensor DHT22 para medir humedad y temperatura y un Sensor de Material Particulado Nova SDS011. Contiene además un RTC DS3231 y almacena los datos en una tarjeta SD.

Puedes revisar el esquemático en [este](/pcb/MPsensor_v1.pdf) documento, y también puedes encontrar los archivos editables en Eagle en la carpeta [pcb](/pcb).

<img src="/img/mp_sch.png" width="700">

### Componentes v1

Los componentes necesarios para esamblar y poner en marcha el dispositivo son los siguientes

|Componente | Cantidad | Link de referencia |
--- | --- | ---
|Arduino Pro Mini| 1 |[Arduino Pro Mini](https://altronics.cl/arduino-pro-mini?search=pro%20mini )|
|RTC DS3231| 1 |[DS3231](https://altronics.cl/modulo-reloj-tiempo-real-rtc-DS3231-AT24C32?search=reloj)|
|Módulo micro SD | 1 | [uSD MOD](https://altronics.cl/modulo-micro-sd-01)|
|Sensor temperatura DHT22 | 1  | [DHT22](https://altronics.cl/sensor-temperatura-humedad-dht22?search=dht22) |
|Sensor MP Nova SDS011 | 1  |[Nova SDS011](https://evoltapc.cl/sensores/2285-sensor-nova-pm2-5-polvo-alta-presicion-alergia-materiial-particulado.html) |
|Conector de alimentación  | 1 | [Conector](https://altronics.cl/par-conectores-dc-locking?search=jack)|
|Fuente de poder 12v  | 1 |  [Fuente 12v](https://altronics.cl/fuente-poder-12V-1-5-a-wall?search=fuente%2012) |

\* Enlaces revisados en marzo de 2022

### Código fuente Arduino

--


## Versión 2

### Circuito electrónico v2

La versión 2 del sensor utiliza un controlador ESP32 que tiene la ventaja de poder conectarse a internet y publicar directamente los datos en un servidor. Además guarda los datos en una tarjeta SD por si la conexión a internet es débil.

Esta versión del dispositivo fue montado en una placa pre-perforada. La PCB dedicada de este dispositivo esta aún en proceso de diseñ

### Componentes v2

Los componentes necesarios para esamblar y poner en marcha el dispositivo son los siguientes

|Componente | Cantidad | Link de referencia |
--- | --- | ---
|Esp32| 1 |[Módulo Esp32 Altronics](https://altronics.cl/tarjeta-esp32-microusb?search=esp32)|
|RTC DS3231| 1 |[DS3231](https://altronics.cl/modulo-reloj-tiempo-real-rtc-DS3231-AT24C32?search=reloj)|
|Módulo SD | 1 | [SD MOD](https://altronics.cl/modulo-sd-01)|
|Sensor temperatura DHT22 | 1  | [DHT22](https://altronics.cl/sensor-temperatura-humedad-dht22?search=dht22) |
|Sensor MP Nova SDS011 | 1  |[Nova SDS011](https://evoltapc.cl/sensores/2285-sensor-nova-pm2-5-polvo-alta-presicion-alergia-materiial-particulado.html) |
|Fuente de poder 5v micro usb  | 1 | [Fuente 5v](https://altronics.cl/power-supply-5v-2A-micro-usb?search=fuente%205v)|


\* Enlaces revisados en marzo de 2022

### Código fuente Arduino

El código fuente del dispositivo fue escrito en Arduino IDE y lo puedes encontrar en la carpeta [src/sensor-cega-v2](src/sensor-cega-v2)

Para programar un ESP32 en Arduino IDE pueden seguir las siguientes [instrucciones](https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/).
